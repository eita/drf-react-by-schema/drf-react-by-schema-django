from django.apps import AppConfig


class DrfReactBySchemaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'drf_react_by_schema'
